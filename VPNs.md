# What is a VPN

What does VPN stand for?

> VPN stands for Virtual Private Network.

So essentially it means that that if you are on an Internet connection (i.e. public wifi) you can connect yourself to a trusted network (i.e. home or work) and browse the Internet as if you were physically there! This is beneficial because a lot of your data might be up for grabs if a network administrator or bad actor wants to gather or steal your information (browsing history, location, password stealing via a [honeypot](https://en.wikipedia.org/wiki/Honeypot_(computing)) or [DNS Spoof](https://en.wikipedia.org/wiki/DNS_spoofing)) or if you are subject to censoring or location based limitations (YouTube/NetFlix/etc.).

A VPN creates what is called a "Tunnel" from an untrusted network (public wifi) to your secure network. This tunnel is encrypted using various protocols (IPSec,L2TP,SSL/TSL,PPTP, and SSH).

## Paying for managed VPN

If you don't run a VPN on your personal network or connect to one for work, it is possible to pay for a service to use. When using a hosted VPN, be aware that it has both advantages and disadvantages. Often times, paid for solutions offer cross platform integration and easy to manage accounts with a variety of VPN locations across the world. BUT your traffic might not be as private as you wish it to be. Since you do not control the servers that you are connecting to, you have no way of knowing what information these providers store. You put good faith in the provider in exchange for the convenience they offer. If you are looking to utilize a service like this, [Mozilla](https://fpn.firefox.com/vpn) is currently beta testing a service for US residents only. [Private Internet Access](https://www.privateinternetaccess.com/) is another well respected provider in the DLN community.

You could also use a virtual private server (VPS) provider to run your own VPN. In this case, you are renting virtual machine space and bandwidth from a third party and you can typically install any software you want on that server. The advantage to this approach is that you can customize your level of privacy and choose your region of deployment. [Digital Ocean](do.co/dl) is a great VPS provider and a sponsor of the DLN community. They offer 1-Click app installation for [OpenVPN](https://en.wikipedia.org/wiki/OpenVPN) and even have a [spin that includes additional ad blocking](https://marketplace.digitalocean.com/apps/openvpn-pihole) by utilizing a project called [Pi-Hole](https://pi-hole.net/).

A new VPN technology called [WireGuard](https://www.wireguard.com/) can also be easily installed on a [Digital Ocean droplet](https://www.digitalocean.com/community/tutorials/how-to-create-a-point-to-point-vpn-with-wireguard-on-ubuntu-16-04) but the final section of this chapter will focus on running your own WireGuard instance.

## Hosting your own VPN

If you have a spare machine sitting around and you want to play with VPN technology, self-hosting is certainly a viable option. Keep in mind that your public IP address will be used when you are on your self-hosted VPN so if you wouldn't want that associated with certain activities, hosting your own VPN would not be a safe guard for that situation. Rather, self-hosting allows you the opportunity to connect to a network you trust and remote access to other resources available on your LAN (local area network).

# Project: Installing WireGuard on your Home Network

This guide assumes you know how to:

- use SSH
- access admin features of your router
- port forward on your router

## Wire Guard Installation On your main device-

1. Find out what your public IP address is by doing a search or going to: www.whatsmyip.org
2. Sign up for [NoIP](www.noip.com) and register a domain name ![](https://imgur.com/n7OfgM9.png)
3. Forward Wire Guard Port on your router to point at the server. **Need to see the port displayed after running the Wire Guard script on the server.**

## On the computer that will be the Wire Guard Server-

1. Install wireguard PPA

  > sudo add-apt-repository ppa:wireguard/wireguard

  > sudo apt-get update

2. Install wireguard and additional packages (debian base)

  > sudo apt install wireguard-tools mawk grep iproute2 qrencode

3. Download the Easy WireGuard Script from [Burghardt's GitHub](https://github.com/burghardt/easy-wg-quick)

  > wget <https://raw.githubusercontent.com/burghardt/easy-wg-quick/master/easy-wg-quick>

4. Edit the script file to replace the Endpoint with the NoIP address nano easy-wg-quick Then, change the EXT_NET_IP section under the main() part of the script:

original configuration ![](https://imgur.com/M1jjE43.png) edited configuration ![](https://imgur.com/2Zzjf4r.png)

5. Make the script executable

  > chmod +x easy-wg-quick

6. Execute the script

  > bash easy-wg-quick ![](https://imgur.com/FgwJN9k.png)

7. Scan code with Wire Guard phone app. NOW YOU NEED TO PORT FORWARD ON YOUR ROUTER. IN THIS EXAMPLE THE PORT IS 35740 BUT YOURS WILL BE DIFFERENT.

8. Restart Server

  > sudo shutdown -r now

9. Make sure wireguard is running

  > sudo wg-quick up ./wghub.conf

10. Check to make sure that your phone connects (turn off wifi, use cell data) ![](https://imgur.com/42mFsjg.png)

# Resources

- [Ask Noah Show #155](https://podcast.asknoahshow.com/155)

- [Mozilla VPN 2019](https://fpn.firefox.com/?utm_source=www.mozilla.org&utm_medium=referral&utm_campaign=homepage&utm_content=card)

- [PC Mag 2019](https://www.pcmag.com/article/352757/you-need-a-vpn-and-heres-why)

- [How to Geek 2019](https://www.howtogeek.com/133680/htg-explains-what-is-a-vpn/)

- [Gizmodo Explanation from 2013](https://gizmodo.com/vpns-what-they-do-how-they-work-and-why-youre-dumb-f-5990192)

- [Digital Ocean- VPS Provider](do.co/dl)- Link supports the DLN network and you get a $50 credit to try out their services :happy:

- [Digital Ocean OpenVPN/Pi-Hole Droplet](https://marketplace.digitalocean.com/apps/openvpn-pihole)
- [Digital Ocean WireGuard Droplet](https://www.digitalocean.com/community/tutorials/how-to-create-a-point-to-point-vpn-with-wireguard-on-ubuntu-16-04)

- [Pi-Hole](https://pi-hole.net/)- Ad blocking project that runs on a variety of hardware

- [WireGuard](https://www.wireguard.com/)- New, sleek, VPN that everyone is talking about.

- Wikipedia links [(Please consider contributing financially to Wikipedia)](https://donate.wikimedia.org/w/index.php?title=Special:LandingPage&country=US&uselang=en&utm_medium=portal&utm_source=portalBanner_en6C_2019c_dsn_2&utm_campaign=portalBanner)

 - [Honeypot](https://en.wikipedia.org/wiki/Honeypot_(computing))
 - [DNS Spoof](https://en.wikipedia.org/wiki/DNS_spoofing)
 - [OpenVPN](https://en.wikipedia.org/wiki/OpenVPN)
