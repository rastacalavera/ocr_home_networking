# OCR_Home_Networking
### Welcome Stranger!

OCR stands for Open Community Resource. I'm not sold on that acronym so if you have a better idea, submit a pull request.
I've been really into the stuff mentioned in Destination Linux and Ask Noah lately so I thought this might be a cool project. 

I kind of thought this could be setup as an eventual eBook that people could view or download and can undergo constant updates as things change
I want to make this for the Destination Linux community and with the Destination Linux community. If the mods like the project
or eventually think that it's a valuable resource, maybe we could put "Endorsed by DLN" or something like that.

# Check out the [WIKI](https://gitlab.com/rastacalavera/ocr_home_networking/-/wikis/%5BOCR%5D-Home-Networking-for-the-Curious-and-the-Serious)

# Interested in getting involved? Want a little incentive? I've got some games you can score:
# Games for Contributions 
By providing well written documentation with references and citations you can pick one or two games from the list and I'll send them your way via STEAM
* Chasm
* Fluffy Horde
* My Time at Portia	
* Regular Human Basketball
* SOULCALIBUR VI
* Sword Legacy Omen
* Yakuza Kiwami
* 11-11 Memories Retold
* Call of Duty®: WWII
* Call of Duty®: WWII - Call of Duty Endowment Bravery Pack
* Call of Duty®: WWII - Call of Duty Endowment Fear Not Pack
* Crash Bandicoot™ N. Sane Trilogy
* Evergarden
* Shenmue I & II
* Spyro™ Reignited Trilogy
* SYNTHETIK: Legion Rising
* Avernum 3: Ruined World
* BATTLETECH
* BATTLETECH - Flashpoint
* BATTLETECH - Shadow Hawk Pack
* Override: Mech City Brawl
* Planet Alpha
* Puss!
* Sonic Mania
* The Spiral Scouts
* God's Trigger
* Guacamelee! 2
* MOTHERGUNSHIP
* Shadowgrounds Survivor
* Apotheon
* NBA 2K17
* Nongünz
* Offworld Trading Company
* ONE PIECE BURNING BLOOD
* Overcooked
* ~~Pillars of Eternity~~
* War for the Overworld
* Wuppo
* DiRT Rally
* GoNNER - Press Jump To Die Edition
* Inside
* Metrico+
* Super Rude Bear Resurrection
* This is the Police
* The Turing Test
* ~~Undertale~~
#### Only 1 of the following can be picked
>* ~~Black Mesa~~
>* Event[0]
>* Kingdom: New Lands Royal Edition
>* Layers of Fear: Masterpiece Edition
>* Slime-san
>* Tumblestone
>* Flat Heroes
>* Morphblade
>* One Piece Pirate Warriors 3
>* Poly Bridge
>* RIVE: Wreck, Hack, Die, Retry
>* Space Run Galaxy
* Total War: WARHAMMER
* Uurnog Uurnlimited
* ABZU
* Husk
* Okhlos
* Project Highrise
* Ryse: Son of Rome
* Steamworld Heist
* XCOM® 2
* HoPiKo

#### Only one of the following can be picked
> * Jotun
> * Kimmy
> * Mother Russia Bleeds
> * ~~Neon Chrome~~
> * Project CARS
> * Warhammer: End Times - Vermintide
> * Warhammer: End Times - Vermintide Schluesselschloss DLC
> * Warhammer: End Times - Vermintide The Outsider DLC
> * Dragon's Dogma: Dark Arisen

* The Escapists - Alcatraz
* The Escapists - Base Game
* The Escapists - Duct Tapes are Forever
* The Escapists - Escape T
* The Escapists - Fhurst Peak
* The Flame in the Flood
* Hacknet (3 copies)
* Minion Masters
* Mordheim City of the Damned
* Western Press
* Western Press Mk Cans II Character DLC
* Human Resource Machine
* Beyond Eyes
* ~~BROFORCE~~
* Kathy Rain
* Pirate Pop Plus
* Rebel Galaxy
* ~~Stardew Valley~~
* Styx: Master of Shadows
* Action Henk
* Deponia Doomsday
* Fidel - Dungeon Rescue
* Grim Dawn
* Hotline Miami 2: Wrong Number
* Slime Rancher
* THOTH
* Train Valley
* The Banner Saga
* Epistory - Typing Chronicles
* Sheltered
* SOMA
* Town of Salem
* WWE 2K16
* Call of Duty®: Black Ops III - Multiplayer Starter Pack
* The Incredible Adventures of Van Helsing: Final Cut
* The Jackbox Party Pack 2
* Planet of the EyesAction Henk
* Poi
* Random Access Murder
* Starward Rogue
* Avernum 2: Crystal Souls
* Copoka
* Cthulhu Realms - Full Version
* Hurtworld
* Kentucky Route Zero
* The Red Solstice
* Satellite Reign
* TIS-100
* Dungeon of the Endless - Crystal Pack
* The Forest

#### Pick one of the following
> * ~~Planetary Annihilation TITANS~~
> * Steredenn
> * Wasted
> * 1993 Space Machine
> * Crawl
> * Fran Bow
> * Galak-Z
> * Gunmetal Arcadia Zero 
> 

* Infinifactory
* JumpJet Rex
* Mad Max
* Oddworld: New 'n' Tasty
* Avalanche 2: Super Avalanche
* The Magic Circle
* Nova-111
* Nuclear Throne
* ~~This War of Mine~~
* BATTLESLOTHS
* I am Bread
* Sentinels of the Multiverse
* Switchcars
* Wasteland 2: Director's Cut - Classic Edition
* Star Wars™ Battlefront™ II (Classic, 2005)
* Star Wars™: Dark Forces
* Star Wars™ Empire At War: Gold Pack
* Star Wars Galactic Battlegrounds Saga™
* Star Wars™ Jedi Knight Dark Forces II
* Star Wars™ Knights of the Old Republic™
* Star Wars™ Knights of the Old Republic™ II: The Sith Lords™
* Star Wars Rebellion™
* Star Wars™: TIE Fighter Special Edition
* Star Wars™: X-Wing Alliance™
* Star Wars™: X-Wing Special Edition
* Star Wars:™ X-Wing vs TIE Fighter - Balance of Power Campaigns™
* Costume Quest
* Grim Fandango Remastered
* Mortal Kombat Kollection
* Batman™: Arkham Origins DLC
* F.E.A.R.
* Gotham City Impostors: Professional Kit
* Guardians of Middle-earth
* Guardians of Middle-earth: Smaug's Treasure DLC
* The Lord of the Rings Online: Steely Dawn Starter